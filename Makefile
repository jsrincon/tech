protoc:
	protoc --go_out=plugins=grpc:internal/common/genproto/reception -I api/protobuf api/protobuf/reception.proto

.PHONY: protoc