package config

import "github.com/pkg/errors"

// Sqldb database code. Need to map to the database code (DataStoreConfig) in the configuration yaml file.
const (
	Sqldb string = "Sqldb"
)

// constant for logger code, it needs to match log code (logConfig)in configuration
const (
	logrus string = "logrus"
	zap    string = "zap"
)

// use case code. Need to map to the use case code (UseCaseConfig) in the configuration yaml file.
// Client app use those to retrieve use case from the container
const (
	Registration   string = "registration"
	RegistrationTx string = "registrationTx"
	ListReception  string = "listreception"
)

// ReceptionData data service code. Need to map to the data service code (DataConfig) in the configuration yaml file.
const (
	ReceptionData string = "receptionData"
)

func validateConfig(appConfig AppConfig) error {
	err := validateDataStore(appConfig)
	if err != nil {
		return errors.Wrap(err, "")
	}
	err = validateLogger(appConfig)
	if err != nil {
		return errors.Wrap(err, "")
	}
	useCase := appConfig.UseCaseConfig
	err = validateUseCase(useCase)
	if err != nil {
		return errors.Wrap(err, "")
	}
	return nil
}

func validateLogger(appConfig AppConfig) error {
	zc := appConfig.ZapConfig
	key := zc.Code
	zcMsg := " in validateLogger doesn't match key = "
	if zap != key {
		errMsg := zap + zcMsg + key
		return errors.New(errMsg)
	}
	lc := appConfig.LogrusConfig
	key = lc.Code
	if logrus != lc.Code {
		errMsg := logrus + zcMsg + key
		return errors.New(errMsg)
	}
	return nil
}

func validateDataStore(appConfig AppConfig) error {
	sc := appConfig.SQLConfig
	key := sc.Code
	scMsg := " in validateDataStore doesn't match key = "
	if Sqldb != key {
		errMsg := Sqldb + scMsg + key
		return errors.New(errMsg)
	}

	return nil
}

func validateUseCase(useCase UseCaseConfig) error {
	err := validateRegistration(useCase)
	if err != nil {
		return errors.Wrap(err, "")
	}
	err = validateRegistrationTx(useCase)
	if err != nil {
		return errors.Wrap(err, "")
	}
	err = validateListUser(useCase)
	if err != nil {
		return errors.Wrap(err, "")
	}
	return nil
}

func validateRegistration(useCaseConfig UseCaseConfig) error {
	rc := useCaseConfig.Registration
	key := rc.Code
	rcMsg := " in validateRegistration doesn't match key = "
	if Registration != key {
		errMsg := Registration + rcMsg + key
		return errors.New(errMsg)
	}
	key = rc.ReceptionDataConfig.Code
	if ReceptionData != key {
		errMsg := ReceptionData + rcMsg + key
		return errors.New(errMsg)
	}
	return nil
}

func validateRegistrationTx(useCaseConfig UseCaseConfig) error {
	rc := useCaseConfig.RegistrationTx
	key := rc.Code
	rcMsg := " in validateRegistrationTx doesn't match key = "
	if RegistrationTx != key {
		errMsg := RegistrationTx + rcMsg + key
		return errors.New(errMsg)
	}
	key = rc.ReceptionDataConfig.Code
	if ReceptionData != key {
		errMsg := ReceptionData + rcMsg + key
		return errors.New(errMsg)
	}
	return nil
}

func validateListUser(useCaseConfig UseCaseConfig) error {
	lc := useCaseConfig.ListReception
	key := lc.Code
	luMsg := " in validateListUser doesn't match key = "
	if ListReception != key {
		errMsg := ListReception + luMsg + key
		return errors.New(errMsg)
	}
	return nil
}
