package app

import (
	config2 "bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container/service"
	"bitbucket.org/jsrincon/tech/internal/reception/app/logger"
	logConfig "github.com/jfeng45/glogger/config"
	logFactory "github.com/jfeng45/glogger/factory"
	"github.com/pkg/errors"
)

func InitApp(filename ...string) (container.Container, error) {
	config, err := config2.BuildConfig(filename...)
	if err != nil {
		return nil, errors.Wrap(err, "BuildConfig")
	}
	err = initLogger(&config.LogConfig)

	if err != nil {
		return nil, err
	}
	return initContainer(config)
}

func initLogger(lc *logConfig.Logging) error {
	log, err := logFactory.Build(lc)
	if err != nil {
		return errors.Wrap(err, "loadLogger")
	}
	logger.SetLogger(log)
	return nil
}

func initContainer(config *config2.AppConfig) (container.Container, error) {
	factoryMap := make(map[string]interface{})
	c := service.Container{FactoryMap: factoryMap, AppConfig: config}

	return &c, nil
}
