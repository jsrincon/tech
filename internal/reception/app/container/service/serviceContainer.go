package service

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container/usecasefactory"
)

type Container struct {
	FactoryMap map[string]interface{}
	AppConfig  *config.AppConfig
}

func (sc Container) BuildUseCase(code string) (interface{}, error) {
	return usecasefactory.GetUseCaseFb(code).Build(sc, sc.AppConfig, code)
}

func (sc Container) Get(code string) (interface{}, bool) {
	value, found := sc.FactoryMap[code]
	return value, found
}

func (sc Container) Put(code string, value interface{}) {
	sc.FactoryMap[code] = value
}
