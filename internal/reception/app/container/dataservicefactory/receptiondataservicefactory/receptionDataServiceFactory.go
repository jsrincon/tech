package receptiondataservicefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice"
)

var udsFbMap = map[string]receptionDataServiceFbInterface{
	config.Sqldb: &sqlReceptionFactory{},
}

// The builder interface for dataservicefactory method pattern
// Every dataservicefactory needs to implement Build method
type receptionDataServiceFbInterface interface {
	Build(container.Container, *config.DataConfig) (dataservice.ReceptionDataInterface, error)
}

// GetDataServiceFb is accessors for factoryBuilderMap
func GetUserDataServiceFb(key string) receptionDataServiceFbInterface {
	return udsFbMap[key]
}
