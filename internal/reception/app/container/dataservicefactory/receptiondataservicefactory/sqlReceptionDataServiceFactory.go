package receptiondataservicefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container/datastorefactory"
	"bitbucket.org/jsrincon/tech/internal/reception/app/logger"
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice"
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice/sqldb"
	"github.com/jfeng45/gtransaction/gdbc"
	"github.com/pkg/errors"
)

type sqlReceptionFactory struct{}

func (srf sqlReceptionFactory) Build(container container.Container, config *config.DataConfig) (dataservice.ReceptionDataInterface, error) {
	logger.Log.Debug("sqlUserDataServiceFactory")
	dsc := config.DataStoreConfig
	dsi, err := datastorefactory.GetDataStoreFb(dsc.Code).Build(container, &dsc)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	ds := dsi.(gdbc.SqlGdbc)
	uds := sqldb.ReceptionDataSql{DB: ds}
	logger.Log.Debug("uds:", uds.DB)
	return &uds, nil
}
