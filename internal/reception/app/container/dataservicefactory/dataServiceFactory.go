package dataservicefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
)

var dsFbMap = map[string]dataServiceFbInterface{
	config.ReceptionData: &receptionDataServiceFactoryWrapper{},
}

// DataServiceInterface serves as a marker to indicate the return type for Build method
type DataServiceInterface interface{}

// The builder interface for dataServiceFbInterface method pattern
// Every dataServiceFbInterface needs to implement Build method
type dataServiceFbInterface interface {
	Build(container.Container, *config.DataConfig) (DataServiceInterface, error)
}

// GetDataServiceFb is accessors for factoryBuilderMap
func GetDataServiceFb(key string) dataServiceFbInterface {
	return dsFbMap[key]
}
