package dataservicefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container/dataservicefactory/receptiondataservicefactory"
	"bitbucket.org/jsrincon/tech/internal/reception/app/logger"
	"github.com/pkg/errors"
)

// receptionDataServiceFactoryWrapper is a empty receiver for Build method
type receptionDataServiceFactoryWrapper struct{}

func (rdsfw *receptionDataServiceFactoryWrapper) Build(c container.Container, dataConfig *config.DataConfig) (DataServiceInterface, error) {
	logger.Log.Debug("UserDataServiceFactory")
	key := dataConfig.DataStoreConfig.Code
	udsi, err := receptiondataservicefactory.GetUserDataServiceFb(key).Build(c, dataConfig)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	return udsi, nil
}
