package datastorefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/app/logger"
	"database/sql"
	databaseConfig "github.com/jfeng45/gtransaction/config"
	"github.com/jfeng45/gtransaction/factory"
	"github.com/jfeng45/gtransaction/gdbc"
)

type sqlFactory struct{}

func (sf sqlFactory) Build(container container.Container, config *config.DataStoreConfig) (DataStoreInterface, error) {
	logger.Log.Debug("sqlFactory")
	key := config.Code
	// Only non-transaction connection is cached
	if !config.Tx {
		if value, found := container.Get(key); found {
			logger.Log.Debug("found db in container for key:", key)
			return value, nil
		}
	}
	tdb := databaseConfig.DatabaseConfig{DriverName: config.DriverName, DataSourceName: config.UrlAddress, Tx: config.Tx}
	db, err := factory.BuildSqlDB(&tdb)
	if err != nil {
		return nil, err
	}
	sqlGdbc, err := buildGdbc(db, config.Tx)
	if err != nil {
		return nil, err
	}
	// Only non-transaction connection is cached
	if !config.Tx {
		container.Put(key, sqlGdbc)
	}
	return sqlGdbc, nil
}

func buildGdbc(sdb *sql.DB, tx bool) (gdbc.SqlGdbc, error) {
	var sdt gdbc.SqlGdbc
	if tx {
		tx, err := sdb.Begin()
		if err != nil {
			return nil, err
		}
		sdt = &gdbc.SqlConnTx{DB: tx}
		logger.Log.Debug("buildGdbc(), create TX:")
	} else {
		sdt = &gdbc.SqlDBTx{DB: sdb}
		logger.Log.Debug("buildGdbc(), create DB:")
	}
	return sdt, nil
}
