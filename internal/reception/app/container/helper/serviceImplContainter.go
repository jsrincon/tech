package helper

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/usecase"
	"github.com/pkg/errors"
)

func GetListReceptionUseCase(c container.Container) (usecase.ListReceptionUseCaseI, error) {
	key := config.ListReception
	value, err := c.BuildUseCase(key)
	if err != nil {
		//logger.Log.Errorf("%+v\n", err)
		return nil, errors.Wrap(err, "")
	}
	return value.(usecase.ListReceptionUseCaseI), nil
}

func GetRegistrationUseCase(c container.Container) (usecase.RegistrationUseCaseI, error) {
	key := config.Registration
	value, err := c.BuildUseCase(key)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	return value.(usecase.RegistrationUseCaseI), nil
}
