package usecasefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/usecase/registration"
	"github.com/pkg/errors"
)

type RegistrationFactory struct {
}

func (rf RegistrationFactory) Build(container container.Container, appConfig *config.AppConfig, key string) (useCaseInterface, error) {
	useCase := appConfig.UseCaseConfig.Registration
	receptionDataInterface, err := buildUserData(container, &useCase.ReceptionDataConfig)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	ruc := registration.UseCase{ReceptionDataInterface: receptionDataInterface}

	return &ruc, nil
}
