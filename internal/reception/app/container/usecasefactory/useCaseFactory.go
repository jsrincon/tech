package usecasefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
)

var BuilderMap = map[string]UseCaseFbInterface{
	config.Registration:  &RegistrationFactory{},
	config.ListReception: &ListReceptionFactory{},
}

type useCaseInterface interface{}

type UseCaseFbInterface interface {
	Build(c container.Container, appConfig *config.AppConfig, key string) (useCaseInterface, error)
}

func GetUseCaseFb(key string) UseCaseFbInterface {
	return BuilderMap[key]
}
