package usecasefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/usecase/listreception"
	"github.com/pkg/errors"
)

type ListReceptionFactory struct{}

func (lrf ListReceptionFactory) Build(container container.Container, appConfig *config.AppConfig, key string) (useCaseInterface, error) {
	useCase := appConfig.UseCaseConfig.ListReception

	receptionDataInterface, err := buildUserData(container, &useCase.ReceptionDataConfig)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	listReceptionUseCase := listreception.UseCase{ReceptionDataInterface: receptionDataInterface}
	return &listReceptionUseCase, nil
}
