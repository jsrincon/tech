package usecasefactory

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/config"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container"
	"bitbucket.org/jsrincon/tech/internal/reception/app/container/dataservicefactory"
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice"
	"github.com/pkg/errors"
)

func buildUserData(c container.Container, dc *config.DataConfig) (dataservice.ReceptionDataInterface, error) {
	dsi, err := dataservicefactory.GetDataServiceFb(dc.Code).Build(c, dc)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	udi := dsi.(dataservice.ReceptionDataInterface)
	return udi, nil
}
