package model

import (
	validation "github.com/go-ozzo/ozzo-validation"
	"time"
)

type Reception struct {
	Id            int
	Reason        string `json:"reason,omitempty"`
	Observation   string `json:"observation,omitempty"`
	Customer      int    `json:"customer,omitempty"`
	Equipment     int    `json:"equipment,omitempty"`
	DateAdmission time.Time
	DateDelivery  time.Time
	CreateAt      time.Time
	UpdateAt      time.Time
}

type Filter struct {
	Key   string `json:"key"`
	Value string `json:"value,omitempty"`
}

func (r Reception) Validate() error {
	return validation.ValidateStruct(&r,
		validation.Field(&r.Reason, validation.Required),
		validation.Field(&r.Observation, validation.Required),
		validation.Field(&r.Customer, validation.Required),
		validation.Field(&r.Equipment, validation.Required))
}

func (r Reception) ValidatePersisted() error {
	return validation.ValidateStruct(&r,
		validation.Field(&r.Id, validation.Required),
		validation.Field(&r.Reason, validation.Required),
		validation.Field(&r.Observation, validation.Required),
		validation.Field(&r.Customer, validation.Required),
		validation.Field(&r.Equipment, validation.Required))
}
