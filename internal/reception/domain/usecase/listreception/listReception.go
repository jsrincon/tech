package listreception

import (
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/model"
)

type UseCase struct {
	ReceptionDataInterface dataservice.ReceptionDataInterface
}

func (lrc UseCase) ListReception() ([]model.Reception, error) {
	return lrc.ReceptionDataInterface.FindAll()
}

func (lrc UseCase) Find(params []model.Filter) ([]model.Reception, error) {
	// Validate Filters
	return lrc.ReceptionDataInterface.Find(params)
}
