package registration

import (
	"bitbucket.org/jsrincon/tech/internal/reception/applicationservice/dataservice"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/model"
	"github.com/pkg/errors"
	"strconv"
)

type UseCase struct {
	ReceptionDataInterface dataservice.ReceptionDataInterface
}

func (ruc UseCase) RegisterReception(reception *model.Reception) (*model.Reception, error) {
	err := reception.Validate()
	if err != nil {
		return nil, errors.Wrap(err, "reception validation failed")
	}
	resultReception, err := ruc.ReceptionDataInterface.Insert(reception)

	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	return resultReception, nil
}

func (ruc UseCase) ModifyReception(reception *model.Reception) error {
	err := reception.ValidatePersisted()
	if err != nil {
		return errors.Wrap(err, "user validation failed")
	}

	rowsAffected, err := ruc.ReceptionDataInterface.Update(reception)

	if err != nil {
		return errors.Wrap(err, "")
	}

	if rowsAffected != 1 {
		return errors.New("Modify user failed. rows affected is " + strconv.Itoa(int(rowsAffected)))
	}

	return nil
}
