package usecase

import "bitbucket.org/jsrincon/tech/internal/reception/domain/model"

// RegistrationUseCaseI is for equipment receptions to be registered in the application
type RegistrationUseCaseI interface {

	//RegisterReception records a receipt associated with a customer and a team
	RegisterReception(reception *model.Reception) (resultReception *model.Reception, err error)

	//ModifyReception change reception data based on the Reception.Id
	ModifyReception(reception *model.Reception) error
}

//ListReceptionUseCaseI handle different ways to retrieve reception information
type ListReceptionUseCaseI interface {

	// ListReception retrieves all reception as an array of Reception
	ListReception() ([]model.Reception, error)

	// Find retrieves a reception based on filter param
	Find(params []model.Filter) ([]model.Reception, error)
}
