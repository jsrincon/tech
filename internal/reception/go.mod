module bitbucket.org/jsrincon/tech/internal/reception

go 1.17

require (
	github.com/go-ozzo/ozzo-validation v3.6.0+incompatible
	github.com/jfeng45/glogger v0.0.0-20200604022340-2e2251ddc0a7
	github.com/jfeng45/gtransaction v0.0.0-20200601024905-babe8d366eed
	github.com/pkg/errors v0.9.1
	gopkg.in/yaml.v2 v2.4.0
)

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/asaskevich/govalidator v0.0.0-20210307081110-f21760c49a8d // indirect
	github.com/konsorten/go-windows-terminal-sequences v1.0.1 // indirect
	github.com/sirupsen/logrus v1.4.2 // indirect
	go.uber.org/atomic v1.5.0 // indirect
	go.uber.org/multierr v1.3.0 // indirect
	go.uber.org/tools v0.0.0-20190618225709-2cfd321de3ee // indirect
	go.uber.org/zap v1.13.0 // indirect
	golang.org/x/lint v0.0.0-20190930215403-16217165b5de // indirect
	golang.org/x/sys v0.0.0-20190422165155-953cdadca894 // indirect
	golang.org/x/tools v0.0.0-20191029190741-b9c20aec41a5 // indirect
	honnef.co/go/tools v0.0.1-2019.2.3 // indirect
)

replace bitbucket.org/jsrincon/tech/internal/commons =>  ../commons/