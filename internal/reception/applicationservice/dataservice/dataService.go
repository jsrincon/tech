package dataservice

import (
	"bitbucket.org/jsrincon/tech/internal/reception/domain/model"
	"github.com/jfeng45/gtransaction/txdataservice"
)

type ReceptionDataInterface interface {
	Find(filter []model.Filter) ([]model.Reception, error)

	FindAll() ([]model.Reception, error)

	Update(reception *model.Reception) (rowAffected int64, err error)

	Insert(reception *model.Reception) (resultReception *model.Reception, err error)

	txdataservice.TxDataInterface
}
