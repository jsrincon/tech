package sqldb

import (
	"bitbucket.org/jsrincon/tech/internal/reception/app/logger"
	"bitbucket.org/jsrincon/tech/internal/reception/domain/model"
	"bitbucket.org/jsrincon/tech/internal/reception/tool/timea"
	"database/sql"
	"github.com/jfeng45/gtransaction/gdbc"
	"github.com/pkg/errors"
	"strconv"
	"time"
)

const (
	findAll string = "select * from reception "
	findBy  string = "select * from reception "
	insert  string = "insert into reception (reason, observation, customer, equipment) values (?,?,?,?) "
	update  string = "update reception set reason = ? , observation = ?, date_delivery = ?, updated_at = ?  "
)

type ReceptionDataSql struct {
	DB gdbc.SqlGdbc
}

func (rds ReceptionDataSql) Find(filter []model.Filter) ([]model.Reception, error) {
	query := rds.builderFilter(findBy, filter)
	rows, err := rds.DB.Query(query)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	rds.closeRow(rows)

	var receptions []model.Reception

	for rows.Next() {
		reception, err := rds.rowsToReception(rows)
		if err != nil {
			return receptions, errors.Wrap(err, "")
		}
		receptions = append(receptions, *reception)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.Wrap(err, "")
	}

	logger.Log.Debug("find reception list:", receptions)
	return receptions, nil
}

func (rds ReceptionDataSql) FindAll() ([]model.Reception, error) {
	rows, err := rds.DB.Query(findAll)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	rds.closeRow(rows)

	var receptions []model.Reception

	for rows.Next() {
		reception, err := rds.rowsToReception(rows)
		if err != nil {
			return receptions, errors.Wrap(err, "")
		}
		receptions = append(receptions, *reception)
	}

	if err = rows.Err(); err != nil {
		return nil, errors.Wrap(err, "")
	}

	logger.Log.Debug("find reception list:", receptions)
	return receptions, nil
}

func (rds ReceptionDataSql) Update(reception *model.Reception) (rowAffected int64, err error) {

	stmt, err := rds.DB.Prepare(update)

	if err != nil {
		return 0, errors.Wrap(err, "")
	}

	rds.closeStmt(stmt)

	res, err := stmt.Exec(reception.Reason, reception.Observation, reception.DateDelivery, time.Now())

	if err != nil {
		return 0, errors.Wrap(err, "")
	}
	rowsAffected, err := res.RowsAffected()

	if err != nil {
		return 0, errors.Wrap(err, "")
	}

	logger.Log.Debug("update: rows affected: ", rowsAffected)
	return rowsAffected, nil
}

func (rds ReceptionDataSql) Insert(reception *model.Reception) (resultReception *model.Reception, err error) {

	stmt, err := rds.DB.Prepare(insert)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	rds.closeStmt(stmt)

	res, err := stmt.Exec(reception.Reason, reception.Observation, reception.Customer, reception.Equipment)
	if err != nil {
		return nil, errors.Wrap(err, "")
	}
	id, err := res.LastInsertId()
	reception.Id = int(id)
	logger.Log.Debug("inserted:", reception)

	return reception, nil
}

func (rds ReceptionDataSql) EnableTx(txFunc func() error) error {
	return rds.DB.TxEnd(txFunc)
}

func (rds ReceptionDataSql) builderFilter(query string, filters []model.Filter) string {
	first := true
	var filterQuery string
	for _, filter := range filters {

		if first {
			filterQuery += ` WHERE $` + strconv.Itoa(len(filter.Key)) + `= $` + strconv.Itoa(len(filter.Value))
			first = false
		} else {
			filterQuery += ` AND $` + strconv.Itoa(len(filter.Key)) + `= $` + strconv.Itoa(len(filter.Value))
		}
	}

	query += filterQuery

	return query
}

func (rds ReceptionDataSql) rowsToReception(rows *sql.Rows) (*model.Reception, error) {
	var da string
	var dd string
	var ca string
	var ua string
	reception := &model.Reception{}
	err := rows.Scan(&reception.Id, &reception.Reason, &reception.Observation, &reception.Customer, &reception.Equipment, &da, &dd, &ca, &ua)

	if err != nil {
		return nil, errors.Wrap(err, "")
	}

	reception.DateAdmission, _ = rds.parseDate(da)
	reception.DateDelivery, _ = rds.parseDate(da)
	reception.CreateAt, _ = rds.parseDate(ca)
	reception.UpdateAt, _ = rds.parseDate(ca)

	logger.Log.Debug("rows to Reception:", reception)
	return reception, nil
}

func (rds ReceptionDataSql) parseDate(ds string) (time.Time, error) {
	date, err := time.Parse(timea.FORMAT_ISO8601_DATE, ds)
	if err != nil {
		return time.Time{}, errors.Wrap(err, "")
	}
	return date, nil
}

func (rds ReceptionDataSql) closeStmt(stmt *sql.Stmt) {
	defer func(stmt *sql.Stmt) {
		err := stmt.Close()
		if err != nil {
		}
	}(stmt)
}

func (rds ReceptionDataSql) closeRow(rows *sql.Rows) {
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
		}
	}(rows)
}
